require './my_app'
#require './my_middleware'
#use Rack::Reloader
#use MyMiddleware
#run lambda {|env|  [200, {'Content-Type' => 'text/plain'}, %w[1 2 3] ] }
#run MyApp.new



######### Rack::Builder#map method #############
require 'logger'
rack_app = Rack::Builder.new do
   use Rack::CommonLogger
   Logger.new('rack.log')
   map '/' do 
      run Proc.new {|env| [200, {'Content-Type' => 'text/html'}, ['This is public page']] }
   end

   map '/secret' do
      use Rack::Auth::Basic, 'Restricted Area' do |user, pass|
         user == 'super' && pass == 'secret'
      end

      map '/' do 
         run Proc.new {|env| [200, {'Content-Type' => 'text/html'}, ['This is a secret page']]}
      end

      map '/files' do
         run Proc.new {|env| [200, {'Content-Type' => 'text/html'}, ['Here are the secret files']]}
      end
   end
end


Rack::Handler::WEBrick.run rack_app, :Port => 9292

